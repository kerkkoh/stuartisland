# Stuart Island

This is the source repository of the DayZ terrain "Stuart Island".


## Workshop

https://steamcommunity.com/sharedfiles/filedetails/?id=1936423383


## Usage

When you regenerate layers in Mapframe properties, in the subtab "Samplers" make sure to set `Desired overlap (px)` to `64` or otherwise you will see tearing in the satellite texture.

Please see https://community.bistudio.com/wiki/DayZ:Terrain_sample as the instructions there apply for this sample as well.
