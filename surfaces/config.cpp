class CfgPatches
{
	class stuartisland_surfaces
	{
		requiredAddons[] = {"DZ_Data", "DZ_Surfaces"};
	};
};

class CfgSurfaces 
{
	class DZ_SurfacesExt;
	class si_grass: DZ_SurfacesExt
	{	
		files = "si_grass_*";
		deflection = 0.1;
		rough = 0.08;
		dust = 0.1;
		friction = 0.85;
		restitution = 0.4;
		soundEnviron = "grass";
		character = "si_grass_character";
		soundHit = "soft_ground";
		audibility = 0.8;
		footDamage = 0.01;
		class Visible
		{
			prone = 0.2;
			kneel = 0.6;
			stand = 0.8;
		};
		impact = "Hit_Grass";
		vpSurface = "Grass";
	};
	class si_sand: DZ_SurfacesExt
	{	
		files = "si_sand_*";
		deflection = 0.4;
		rough = 0.05;
		dust = 0.25;
		friction = 0.9;
		restitution = 0.39;
		soundEnviron = "sand";
		character = "Empty";
		soundHit = "hard_ground";
		audibility = 1.2;
		footDamage = 0.5;
		impact = "Hit_Sand";
		vpSurface = "Gravel";
	};
};

class CfgSurfaceCharacters
{
	class si_grass_character
	{
		probability[] = {0.1,0.0,0.01,0.2,0.3};
		names[] = {"ElytrigiaDirt","Lolium","Taraxacum","DirtGrass", "Elytrigia"};
	};
};

class CfgSoundTables
{
	class CfgStepSoundTables
	{
		#include "CfgStepSoundTables.hpp"
	};
};
