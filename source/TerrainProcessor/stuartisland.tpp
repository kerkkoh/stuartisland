<?xml version="1.0" encoding="UTF-8"?>
<Project>
    <Version>79</Version>
    <Name>stuartisland</Name>
    <ExportSettings>
        <Directory></Directory>
        <ExportLBT>1</ExportLBT>
        <ExportTXT>1</ExportTXT>
        <ExportSHP>0</ExportSHP>
    </ExportSettings>
    <Tasks>
        <Task>
            <Shapefile>shapes\bothforests.shp</Shapefile>
            <TaskName>Area: Random</TaskName>
            <DEMInputFilename></DEMInputFilename>
            <DEMOutputFilename></DEMOutputFilename>
            <Enabled>1</Enabled>
            <Notes>Notes</Notes>
            <Parameters>
                <Parameter Name="Random seed" Type="0" Value="100"/>
                <Parameter Name="Hectare density" Type="0" Value="200"/>
            </Parameters>
            <ObjectPrototypes>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="t_PiceaAbies_3f"/>
                    <Parameter Name="PROB" Value="35"/>
                    <Parameter Name="MINHEIGHT" Value="80"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="2"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="t_PiceaAbies_2f"/>
                    <Parameter Name="PROB" Value="20"/>
                    <Parameter Name="MINHEIGHT" Value="80"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="2"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="t_PiceaAbies_2s"/>
                    <Parameter Name="PROB" Value="30"/>
                    <Parameter Name="MINHEIGHT" Value="80"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="2"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="t_PiceaAbies_3s"/>
                    <Parameter Name="PROB" Value="2"/>
                    <Parameter Name="MINHEIGHT" Value="40"/>
                    <Parameter Name="MAXHEIGHT" Value="90"/>
                    <Parameter Name="MINDIST" Value="2"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="t_PiceaAbies_1f"/>
                    <Parameter Name="PROB" Value="15"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="1"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="t_PiceaAbies_1s"/>
                    <Parameter Name="PROB" Value="15"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="1"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="d_piceaabies_stump"/>
                    <Parameter Name="PROB" Value="2"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="2"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="d_piceaabies_stumpb"/>
                    <Parameter Name="PROB" Value="2"/>
                    <Parameter Name="MINHEIGHT" Value="100"/>
                    <Parameter Name="MAXHEIGHT" Value="100"/>
                    <Parameter Name="MINDIST" Value="2"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="d_piceaabies_fallenb"/>
                    <Parameter Name="PROB" Value="3"/>
                    <Parameter Name="MINHEIGHT" Value="100"/>
                    <Parameter Name="MAXHEIGHT" Value="100"/>
                    <Parameter Name="MINDIST" Value="3"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="d_piceaabies_fallen"/>
                    <Parameter Name="PROB" Value="3"/>
                    <Parameter Name="MINHEIGHT" Value="100"/>
                    <Parameter Name="MAXHEIGHT" Value="100"/>
                    <Parameter Name="MINDIST" Value="3"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="t_piceaabies_2d"/>
                    <Parameter Name="PROB" Value="2"/>
                    <Parameter Name="MINHEIGHT" Value="80"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="2"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="t_piceaabies_3d"/>
                    <Parameter Name="PROB" Value="2"/>
                    <Parameter Name="MINHEIGHT" Value="80"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="2"/>
                </ObjectPrototype>
            </ObjectPrototypes>
            <AdditionalDatabases/>
        </Task>
        <Task>
            <Shapefile>shapes\bothforests.shp</Shapefile>
            <TaskName>Area: Random</TaskName>
            <DEMInputFilename></DEMInputFilename>
            <DEMOutputFilename></DEMOutputFilename>
            <Enabled>1</Enabled>
            <Notes>Notes</Notes>
            <Parameters>
                <Parameter Name="Random seed" Type="0" Value="424"/>
                <Parameter Name="Hectare density" Type="0" Value="20"/>
            </Parameters>
            <ObjectPrototypes>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="rock_apart1"/>
                    <Parameter Name="PROB" Value="2"/>
                    <Parameter Name="MINHEIGHT" Value="60"/>
                    <Parameter Name="MAXHEIGHT" Value="70"/>
                    <Parameter Name="MINDIST" Value="5"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="rock_apart2"/>
                    <Parameter Name="PROB" Value="2"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="110"/>
                    <Parameter Name="MINDIST" Value="5"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="stone1"/>
                    <Parameter Name="PROB" Value="20"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="140"/>
                    <Parameter Name="MINDIST" Value="5"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="stone2"/>
                    <Parameter Name="PROB" Value="20"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="140"/>
                    <Parameter Name="MINDIST" Value="5"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="stone3"/>
                    <Parameter Name="PROB" Value="20"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="140"/>
                    <Parameter Name="MINDIST" Value="5"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="stone4"/>
                    <Parameter Name="PROB" Value="20"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="140"/>
                    <Parameter Name="MINDIST" Value="5"/>
                </ObjectPrototype>
                <ObjectPrototype>
                    <Parameter Name="OBJECT" Value="stone5"/>
                    <Parameter Name="PROB" Value="20"/>
                    <Parameter Name="MINHEIGHT" Value="90"/>
                    <Parameter Name="MAXHEIGHT" Value="140"/>
                    <Parameter Name="MINDIST" Value="5"/>
                </ObjectPrototype>
            </ObjectPrototypes>
            <AdditionalDatabases/>
        </Task>
        <Task>
            <Shapefile>shapes\roads.shp</Shapefile>
            <TaskName>Mask: Linear</TaskName>
            <DEMInputFilename></DEMInputFilename>
            <DEMOutputFilename></DEMOutputFilename>
            <Enabled>1</Enabled>
            <Notes>Notes</Notes>
            <Parameters>
                <Parameter Name="Objects distance" Type="0" Value="7"/>
                <Parameter Name="Spline interpolation" Type="0" Value="0"/>
            </Parameters>
            <ObjectPrototypes/>
            <AdditionalDatabases/>
        </Task>
        <Task>
            <Shapefile>shapes\smallerroads.shp</Shapefile>
            <TaskName>Mask: Linear</TaskName>
            <DEMInputFilename></DEMInputFilename>
            <DEMOutputFilename></DEMOutputFilename>
            <Enabled>1</Enabled>
            <Notes>Notes</Notes>
            <Parameters>
                <Parameter Name="Objects distance" Type="0" Value="6"/>
                <Parameter Name="Spline interpolation" Type="0" Value="0"/>
            </Parameters>
            <ObjectPrototypes/>
            <AdditionalDatabases/>
        </Task>
        <Task>
            <Shapefile>shapes\forestmask.shp</Shapefile>
            <TaskName>Mask: Area</TaskName>
            <DEMInputFilename></DEMInputFilename>
            <DEMOutputFilename></DEMOutputFilename>
            <Enabled>1</Enabled>
            <Notes>Notes</Notes>
            <Parameters>
                <Parameter Name="Remove objects" Type="0" Value="Inside"/>
            </Parameters>
            <ObjectPrototypes/>
            <AdditionalDatabases/>
        </Task>
    </Tasks>
</Project>
