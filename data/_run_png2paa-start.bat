for /F "Tokens=2* skip=2" %%A In ('REG QUERY "HKEY_CURRENT_USER\SOFTWARE\Bohemia Interactive\imagetopaa" /v "Tool"') DO SET ImageToPAAPath=%%B

rem mask - quick, as they are simple images with few colors
START "" /D"%CD%" /B "%ImageToPAAPath%" layers\m_*.png

rem satellite texture
rem 1st
START "" /D"%CD%" /B "%ImageToPAAPath%" layers\s_00*.png

rem 2nd
START "" /D"%CD%" /B "%ImageToPAAPath%" layers\s_01*.png

rem norm texture
rem 1st
START "" /D"%CD%" /B "%ImageToPAAPath%" layers\n_00*.png

rem 2nd
START "" /D"%CD%" /B "%ImageToPAAPath%" layers\n_01*.png